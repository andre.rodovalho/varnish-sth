FROM varnish:fresh

RUN apt-get update \
    && apt-get install -y curl nano supervisor openssh-sftp-server openssh-client rsyslog rsync procps cron iputils-ping net-tools \

# SiteHost standards
    && mkdir -p /container/config \
    && userdel www-data \
    && usermod varnish -u 33 \
    && groupmod varnish -g 33  \
    && rm -rf /etc/varnish \
    && rm -rf /etc/supervisor \
    && rm -rf /etc/rsyslog* \
    && mkdir -p /etc/supervisor/conf.d \
    && ln -s /container/config/supervisord.conf /etc/supervisor/supervisord.conf \
    && ln -s /etc/varnish /container/config/varnish \
    && ln -s /var/lib/varnish /container/varnish \

# Cleanup
    && rm -rf /var/lib/apt/lists/*

COPY crontab /etc/crontab
RUN chmod 644 /etc/crontab

EXPOSE 80

ENTRYPOINT ["/usr/bin/supervisord", "-c", "/container/config/supervisord.conf"]
