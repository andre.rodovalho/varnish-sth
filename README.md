# SiteHost - Custom Images
Hello! Before getting started there a couple of small things you need to know about Cloud Container custom images.

### Read Only Files
Due to the tight integration of our automated build and deployment process we do not allow the following files to be edited:
 - .gitlab-ci.yml
 - README.md

### Questions or Problems?
If you get a little stuck at any point our Knowledge Base is a good place to start. You’ll find articles on the various aspects of custom images and some good explanations of how to troubleshoot problems – http://kb.sitehost.co.nz/cloud-containers-custom-images

If you can’t find your answer there and are having problems creating, building or deploying a custom image, please get in touch with our support team - support@sitehost.co.nz